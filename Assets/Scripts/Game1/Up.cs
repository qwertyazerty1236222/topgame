﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Up : MonoBehaviour
{
    public GameObject click;
    public Text score;
    public GameObject shtanga;
    private Vector3 startPos;
    private float timer;
    private float timer2 = 1.2f;
    // Start is called before the first frame update
    void Start()
    {
        startPos = shtanga.transform.position;
        timer = 60;
    }




    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            GlobalDefines.returnsFirstGame = int.Parse(score.text);
            SceneManager.LoadScene("SampleScene");
        }
        if (shtanga.transform.position.y < -0.9)
        {
            click.SetActive(true);
        }
    }
    void UpKey()
    {
        Vector3 position = shtanga.transform.position;
        Debug.Log(position);
        if (position.y > -0.3)
        {
            score.text = (int.Parse(score.text) + 1).ToString();

            click.SetActive(false);
            //shtanga.transform.position = startPos;

        }
        else
        {
            position += new Vector3(0, 0.2f, 0);
            shtanga.transform.position = position;
        }

    }
}
