﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class NewBehaviourScript : MonoBehaviour
{
    private float AllCounter = -89.21f;
    public GameObject[] maps;
    private GameObject Player;
    private float speed;
    private float multip;
    private bool Jumping = false;
    public Text score;
    public GameObject GameOverBG;
    private bool dead;
    private float tmpCounter = 0;
    System.Random rnd = new System.Random();
    float timer = 10f;
    void Start()
    {
       Player = (GameObject)this.gameObject;
        speed = 0.08f;
        multip = 1f;
        dead = false;
    }
    void FixedUpdate()
    {
        if (!dead)
        {
            tmpCounter += speed;
            if(tmpCounter >= 1)
            {
                score.text = (int.Parse(score.text) + 1).ToString();
                tmpCounter = 0;
            }

            
            Player.transform.localPosition += new Vector3(speed * multip, 0, 0);
            Camera.main.transform.localPosition = new Vector3(Player.transform.localPosition.x + 2, 0, -10);
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Jumping == false)
                {
                    GetComponent<Rigidbody>().AddForce(new Vector3(0, speed * 210, 0), ForceMode.Impulse);
                    Jumping = true;
                }
            }
        }
        else
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                GlobalDefines.returnsSecondGame = int.Parse(score.text);
                SceneManager.LoadScene("SampleScene");
            }
        }
    }
    void OnCollisionEnter()
    {
        Jumping = false;

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Respawn")
        {
            dead = true;
            Camera.main.transform.position = new Vector3(GameOverBG.transform.position.x, GameOverBG.transform.position.y, -10);
        }
        else if (other.tag == "ScorePlus")
        {
            score.text = (int.Parse(score.text) + 10).ToString();
        }
        else if(other.tag == "CheckPoint")
        {
            multip += 0.1f;
            InsertMap();
        }
    }
    void InsertMap()
    {
        
        AllCounter += 29.91f;
        int rndmap = rnd.Next(0,maps.Length);
        var map = Instantiate(maps[rndmap]);
        Debug.Log(map);
        map.transform.position = new Vector3(AllCounter, -0.6570272f, 0);
    }
}