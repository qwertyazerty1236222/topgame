﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SetWin : MonoBehaviour
{

    public Text Player;
    public Text Score;
    private float timer = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Player.text = GlobalDefines.RecWinner;
        Score.text = GlobalDefines.RecWinScore.ToString();
        timer -= Time.deltaTime;
        if(timer<=0)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
