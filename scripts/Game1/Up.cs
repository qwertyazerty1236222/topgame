﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Up : MonoBehaviour
{
    public GameObject click;
    public Text score;
    public GameObject shtanga;
    private Vector3 startPos;
    private float timer;
    private float timer2 = 1.2f;
    public GameObject panel;
    public Text time;
    // Start is called before the first frame update
    void Start()
    {
        startPos = shtanga.transform.position;
        timer = 20;
    }




    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
            time.text = Mathf.RoundToInt(timer).ToString();
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            GlobalDefines.WaitPlayerForExit = true;
            GlobalDefines.returnsFirstGame = int.Parse(score.text);
            StartCoroutine(Connect.loadOut(this, 1));
        }
        if (shtanga.transform.position.y < -0.9)
        {
            click.SetActive(true);
        }
        if (GlobalDefines.WaitPlayerForExit)
        {
            panel.SetActive(true);
        }
        else
            panel.SetActive(false);

    }
    void UpKey()
    {
        Vector3 position = shtanga.transform.position;
        Debug.Log(position);
        if (position.y > -0.3)
        {
            score.text = (int.Parse(score.text) + 1).ToString();

            click.SetActive(false);
            //shtanga.transform.position = startPos;

        }
        else
        {
            position += new Vector3(0, 0.2f, 0);
            shtanga.transform.position = position;
        }

    }
}
