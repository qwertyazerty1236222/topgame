﻿using Assets.KeyValueOnlineStorage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CloudSave : MonoBehaviour
{
    public static IEnumerator POST(string Game, int Room, string score)
    {
        var Data = new WWWForm();
        Data.AddField("game", Game);
        Data.AddField("room", Room.ToString());
        Data.AddField("score", score);
        Data.AddField("id", PlayFabLogin.ReturnMobileID());
        var Query = new WWW("http://mevlme44.myjino.ru/score.php/", Data.data);
        yield return Query;
        if (Query.error != null)
        {
            Debug.Log("Server does not respond : " + Query.error);
        }
        Query.Dispose();
    }
    
}

