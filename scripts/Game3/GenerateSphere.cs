﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateSphere : MonoBehaviour
{
    [SerializeField] private Text Score;
    [SerializeField] private GameObject Sphere;
    private int counter = 0;
    void Update()
    {
        if(int.Parse(Score.text)%5 == 0 && int.Parse(Score.text) != counter )
        {
            counter+=5;
            var sp = Instantiate(Sphere);
            sp.transform.position = gameObject.transform.position;
        }
    }
}
