﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapSphere : MonoBehaviour
{
    [SerializeField] private Text Score;
    System.Random rnd = new System.Random();
    public void OnMouseDown()
    {
        if (!GlobalDefines.WaitPlayerForExit)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(rnd.Next(-5, 5), 25, 0), ForceMode.Impulse);
            Score.text = (int.Parse(Score.text) + 1).ToString();
        }
    }
}
