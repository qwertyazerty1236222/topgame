﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Swipe : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{

    private Vector2 Origin;
    private Vector2 Direction;


    public GameObject camera;
    public float sensX = 3f;
    public float sensY = 3f;
    private Quaternion OriginalRot;

    private void Awake()
    {
        Direction = Vector2.zero;
        OriginalRot = camera.transform.localRotation;
    }
    public void OnPointerDown(PointerEventData eventdata)
    {
        Debug.Log("{EQ");
        Origin = eventdata.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OriginalRot = camera.transform.localRotation;
        Direction = Vector2.zero;

    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 currentPozition = eventData.position;
        Vector2 directionRaw = currentPozition - Origin;
        Direction = directionRaw;

    }
    // 
    void Update()
    {

        Quaternion Xqua = Quaternion.AngleAxis(Direction.x, Vector3.up);
        Quaternion Yqua = Quaternion.AngleAxis(Direction.y, Vector3.left);
        camera.transform.localRotation = OriginalRot * Xqua * Yqua;



    }
}