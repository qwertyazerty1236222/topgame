﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalDefines : MonoBehaviour
{
    public static int returnsFirstGame;
    public static int returnsSecondGame;
    public static int returnsThirdGame;
    public static string Key;
    public static int RecRoom;
    public static string RecPlayer1;
    public static string RecPlayer2;
    public static string RecPlayer3;
    public static string RecPlayer4;
    public static string RecPlayer5;
    public static bool WaitPlayer;
    public static bool WaitPlayerForExit;
    public static string RecWinner;
    public static int RecWinScore;
    public static string Name;
    public static bool Loaded;
    public static bool FirstLogin;
    public static string Que;
    public static int RecGame;
    public static bool Save;
    public static bool Win;
    public static int Money;
}
