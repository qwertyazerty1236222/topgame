﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class but : MonoBehaviour
{
    public GameObject LoadPanel;
    public GameObject panel;
    public GameObject SettingsPanel;
    public InputField nameField;
    public InputField nameFieldFirst;
    public Text ID;
    public Text Money;
    public GameObject camera1;
    public GameObject NamePanel;
    public GameObject PayPanel1;
    public GameObject PayPanel2;
    public GameObject PayPanel3;
    public GameObject NotMoney;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Connect.FirstLog(PlayFabLogin.ReturnMobileID()));
        GlobalDefines.Loaded = false;
        ID.text = PlayFabLogin.ReturnMobileID();

    }

    // Update is called once per frame
    void Update()
    {
        if(!GlobalDefines.Loaded)
        {
            LoadPanel.SetActive(true);
        }
        else
        {
            LoadPanel.SetActive(false);
        }
        if(GlobalDefines.WaitPlayer)
        {
            panel.SetActive(true);
        }
        else
        {
            panel.SetActive(false);
        }
        if(GlobalDefines.FirstLogin && GlobalDefines.Loaded)
        {
            NamePanel.SetActive(true);
        }
        else
        {
            NamePanel.SetActive(false);
        }
    }
    void Game()
    {
        if (CheckMoney())
        {
            PayPanel1.SetActive(false);
            GlobalDefines.RecGame = 1;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 1));
            camera1.GetComponent<CameraMovement>().enabled = true;
        }
        else
            NotEnoughMoney(1);
    }
    void Game2()
    {
        if (CheckMoney())
        {
            PayPanel2.SetActive(false);
            GlobalDefines.RecGame = 2;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 2));
            camera1.GetComponent<CameraMovement>().enabled = true;
        }
        else
            NotEnoughMoney(2);
    }

    void Game3()
    {
        if (CheckMoney())
        {
            PayPanel3.SetActive(false);
            GlobalDefines.RecGame = 3;
            GlobalDefines.WaitPlayer = true;
            StartCoroutine(Connect.loadIn(this, 3));
            camera1.GetComponent<CameraMovement>().enabled = true;
        }
        else
            NotEnoughMoney(3);
    }

    void Settings()
    {
        camera1.GetComponent<CameraMovement>().enabled = false;
        SettingsPanel.SetActive(true);
    }

    void ExitSettings()
    {
        camera1.GetComponent<CameraMovement>().enabled = true;
        SettingsPanel.SetActive(false);
    }

    void ChangeName()
    {
        StartCoroutine(Connect.Change(PlayFabLogin.ReturnMobileID(),nameField.text));
    }
    void ChangeNameFirst()
    {
        GlobalDefines.FirstLogin = false;
        StartCoroutine(Connect.Change(PlayFabLogin.ReturnMobileID(), nameFieldFirst.text));

    }
    void Cancel()
    {
        StopAllCoroutines();
        StartCoroutine(Connect.ExitLoad(this, GlobalDefines.RecGame));
        GlobalDefines.WaitPlayer = false;
    }
    public void EnterGame(int game)
    {
        camera1.GetComponent<CameraMovement>().enabled = false;
        if (game == 1)
            PayPanel1.SetActive(true);
        else if (game == 2)
            PayPanel2.SetActive(true);
        else if (game == 3)
            PayPanel3.SetActive(true);
    }
    public void NotEnoughMoney(int game)
    {
        StartCoroutine(anim(NotMoney));
        camera1.GetComponent<CameraMovement>().enabled = true;
        if (game == 1)
            PayPanel1.SetActive(false);
        else if (game == 2)
            PayPanel2.SetActive(false);
        else if (game == 3)
            PayPanel3.SetActive(false);
    }
    public bool CheckMoney()
    {
        if (int.Parse(Money.text) >= 50)
        {
            return true;
        }
        else
            return false;
    }
    public void OnApplicationQuit()
    {

        StartCoroutine(Connect.Disconnect(this, PlayFabLogin.ReturnMobileID(), GlobalDefines.RecGame, 0));
    }
    public IEnumerator anim(GameObject a)
    {
        a.SetActive(true);
        yield return new WaitForSeconds(6);
        a.SetActive(false);
    }

}
