﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayFabLogin : MonoBehaviour
{
    const string ID = "7FC64";
    public Text Money;
    private string myID;
    // Start is called before the first frame update
    void Start()
    {
        Debug.ClearDeveloperConsole();
        LoginIn();
    }
    public static string ReturnMobileID()
    {
        string deviceID = SystemInfo.deviceUniqueIdentifier;
        return deviceID;
    }
    public void LoginIn()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = ID;
        }
        PlayFabClientAPI.LoginWithAndroidDeviceID(new LoginWithAndroidDeviceIDRequest
        {
            CreateAccount = true,
            AndroidDeviceId = ReturnMobileID(),
            TitleId = ID,
            
        }, 
        onLoginSuccess, 
        onLoginFailure);
    }
    public void GetPlayerData()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = myID,
            Keys = null
        }, UserDataSuccess, onLoginFailure);
    }
    private void onLoginSuccess(LoginResult result)
    {
        myID = result.PlayFabId;
        GetPlayerData();
        //PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
        //{
        //    DisplayName = Login.text

        //}, result =>
        //{
        //    GlobalDefines.Name = Login.text;
        //    Debug.Log("The player's display name is now: " + result.DisplayName);
        //}, onLoginFailure);
    }
    public void SetUserData()
    {
        string MoneyData = Money.text;
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                //key value pair, saving the allskins array as a string to the playfab cloud
                {"money", MoneyData}
            }
        }, SetDataSuccess, onLoginFailure);
    }

    void UserDataSuccess(GetUserDataResult result)
    {
        if (result.Data == null || !result.Data.ContainsKey("money"))
        {
            GlobalDefines.Loaded = true;
            Money.text = "0";
        }
        else
        {

            GlobalDefines.Loaded = true;
            Money.text = result.Data["money"].Value;
        }
    }
    void SetDataSuccess(UpdateUserDataResult result)
    {
        Debug.Log("YEEES"+result.DataVersion);
    }
    private void onLoginFailure(PlayFabError error)
    {
        GlobalDefines.Loaded = false;
    }
    // Update is called once per frame
    void Update()
    {
         if(GlobalDefines.Save)
         {
            Money.text = (int.Parse(Money.text) - 50).ToString();
            SetUserData();
            GlobalDefines.Save = false;
         }
         if(GlobalDefines.Win)
         {
            Money.text = (int.Parse(Money.text) + 200).ToString();
            SetUserData();
            GlobalDefines.Win = false;
         }
    }
    
}
